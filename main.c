
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "calrep.h"

/**
 *  Gets program usage
 *  @param[in] name: program name.
 */
void usage(const char *name)
{
    const char *progname = strrchr(name, '/');
    if (NULL == progname) {
        progname = strrchr(name, '\\');
        if (NULL == progname) {
            progname = name;
        } else {
            progname++;
        }
    } else {
        progname++;
    }
    printf("usage: %s [options] [%%d %%m [%%y]]\n", progname);
    printf("options are\n");
    printf("    --wday: display week day\n");
    printf("    --nday: display day name\n");
}



int main(int argc, char *argv[])
{
    time_t date = time(NULL);
    struct tm *tm = localtime(&date);
    int weekday = 0;
    int dayname= 0;

    int i;
    for (i = 1; i < argc; ++i) {
        if (0 == strcmp(argv[i], "--wday")) {
            weekday = 1;
            continue;
        }
        if (0 == strcmp(argv[i], "--nday")) {
            dayname = 1;
            continue;
        }
        if (argc == i) break;

        if (argc == i + 2) {
            if (1!= sscanf(argv[i], "%ud", &tm->tm_mday))
                goto ERROR;
            if (1!= sscanf(argv[i+1], "%ud", &tm->tm_mon))
                goto ERROR;
            tm->tm_mon--;
            date = mktime(tm);
            break;
        }
        if (argc == i + 3) {
            int tmp;
            if (1!= sscanf(argv[i+2], "%ud", &tmp))
                goto ERROR;
            if (tmp < 50) {
                tmp += 2000;
            } else if (tmp < 100) {
                tmp += 1900;
            }
            tm->tm_year = tmp - 1900;
            if (1!= sscanf(argv[i], "%ud", &tm->tm_mday))
                goto ERROR;
            if (1!= sscanf(argv[i+1], "%ud", &tm->tm_mon))
                goto ERROR;
            tm->tm_mon--;
            date = mktime(tm);
            break;
        }
ERROR:
        usage(*argv);
        exit(0);
    }

    set_date(date);

    printf("%s", get_date(weekday, dayname));


    return EXIT_SUCCESS;
}
