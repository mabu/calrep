/**
 * @file roman.h
 */

#ifndef H_20110624_ROMAN
#define H_20110624_ROMAN
 
/**
 *  Converts an arabic into a roman number
 *  @param[in] number: number to convert
 *  @return a static character string.
 *  @warning not thread safe.
 */
const char * arabic2roman(int number);
 
#endif
