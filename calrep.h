/**
 * @file calRep.h
 */

#ifndef H_20110624_CALREP
#define H_20110624_CALREP
void set_date(time_t date);
const char * get_date(int, int);
#endif
