#include <string.h>
#include <stdio.h>
#include <time.h>
#include "roman.h"
#include "days.h"

#define TRACE() printf(__FUNCTION__"\n")

/**
 * reference_time:
 * starting date of republican year in current civil year.
 */
static time_t reference_time;

/**
 * date to convert
 */
static time_t date_to_convert;

const char special_days[][11] = {
  "vertu",
  "genie",
  "travail",
  "opinion",
  "recompense",
  "revolution",
};

const char month_names[][12] = {
    "vendemiaire",
    "brumaire",
    "frimaire",
    "nivose",
    "pluviose",
    "ventose",
    "germinal",
    "floreal",
    "prairial",
    "messidor",
    "thermidor",
    "fructidor",
};

const char day_names[][9] = {
    "primidi",
    "duodi",
    "tridi",
    "quartidi",
    "quintidi",
    "sextidi",
    "septidi",
    "octidi",
    "nonidi",
    "decadi"
};

/**
 *  Compute reference for given date.
 */
void set_date(time_t date)
{
  struct tm * tm = localtime(&date);
  date_to_convert = date;
  if(((8 == tm->tm_mon)&&(22 > tm->tm_mday))||
     (8 >  tm->tm_mon)) {
    //tm->tm_year++;
    tm->tm_year--;
  }
  //tm->tm_year--;
  tm->tm_hour = 0;
  tm->tm_min = 0;
  tm->tm_sec = 0;
  tm->tm_mon = 8;
  tm->tm_mday = 22;
  reference_time = mktime(tm);
}

/**
 *
 */
static int get_month()
{
  int diff;
  /* get difference between dates */
  diff = difftime(date_to_convert, reference_time);

  /* convert seconds in republican months */
  diff /= (60*60*24*30);
  return diff;
}

static int get_year()
{
  struct tm * tm = localtime(&reference_time);
  return 109 + tm->tm_year;
}

static int get_day()
{
  int diff;
  diff = difftime(date_to_convert, reference_time);
  diff /= (60*60*24);
  diff ++;
  diff %= 30;
  if((0 == diff) && (12 != get_month())) {
    diff = 30;
  }
  return diff;
}

static char buffer[512] = "";

const char * get_date(int wday, int nday)
{
    buffer[0] = '\0';
    if(12 == get_month()) {
        /* end of year */
        strcat(buffer, special_days[get_day()-1]);
        strcat(buffer, " ");
        strcat(buffer, arabic2roman(get_year()));
    } else {
        int day = get_day();
        int month = get_month();
        int year = get_year();
        char sday[3];
        sprintf(sday, "%d", day);
        if (wday) {
            strcat(buffer, day_names[(day-1)%10]);
            strcat(buffer, " ");
        }
        strcat(buffer, sday);
        strcat(buffer, " ");
        strcat(buffer, month_names[month]);
        strcat(buffer, " ");
        strcat(buffer, arabic2roman(year));
        if (nday) {
            strcat(buffer, "\n");
            strcat(buffer, days[month][day-1]);
        }
    }
    strcat(buffer, "\n");
  return buffer;
}
