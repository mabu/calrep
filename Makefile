CFLAGS= -O2 -Wall -Wextra  

SRC=$(wildcard *.c)
OBJ=$(patsubst %.c,%.o,$(SRC))

all: calrep

%.o: %.c days.h
	$(CC) $(CFLAGS) $< -c -o $@

calrep: $(OBJ)
	$(CC) $^ -o $@


