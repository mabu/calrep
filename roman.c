#include <stdio.h>
#include "roman.h"

const char units[][5] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
const char decimals[][5] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC", "C"};
const char hundreds[][5] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
const char thousands[][5] = {"", "M", "MM", "MMM", "MMMM"};

char buffer[32];

const char * arabic2roman(int number)
{
    int unit = number%10;
    int decimal = (number/10)%10;
    int hundred = (number/100)%10;
    int thousand = (number/1000)%5;
    sprintf(buffer, "%s%s%s%s", thousands[thousand], hundreds[hundred], decimals[decimal], units[unit]);
    
    return buffer;
}
